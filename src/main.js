import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
import 'element-ui/lib/theme-chalk/index.css'
import {
	Row,
	Col,
	Button,
	Message,
	Dialog
} from 'element-ui'

Vue.use(Row);
Vue.use(Col);
Vue.use(Button);
Vue.use(Dialog);
Vue.prototype.$message = Message

Vue.config.productionTip = false
router.afterEach(route => {
    document.title = route.meta.title + ' | 聚象传媒';
})

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
