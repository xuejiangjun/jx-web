import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    routes: [{
        path: '/',
        name: 'index',
        redirect: '/home',
        hidden: true
    }, {
        path: '/home',
        name: 'home',
        meta: {
            title: '主页'
        },
        component: () =>
            import ('./views/Home.vue')
    }, {
        path: '/video',
        name: 'video',
        meta: {
            title: '视频'
        },
        component: () =>
            import ('./views/Video.vue')
    }, {
        path: '/service',
        name: 'service',
        meta: {
            title: '服务'
        },
        component: () =>
            import ('./views/Service.vue')
    }, {
        path: '/about',
        name: 'about',
        meta: {
            title: '关于我们'
        },
        component: () =>
            import ('./views/About.vue')
    }, {
        path: '/contact',
        name: 'contact',
        meta: {
            title: '联系我们'
        },
        component: () =>
            import ('./views/Contact.vue')
    }]
})
