import axios from 'axios'

var ajax = axios.create({
    baseURL: '//api.filmwhite.com',
    headers: {},
    withCredentials: true,
    crossDomain: true
})

//添加一个请求拦截器
ajax.interceptors.request.use(function(config) {
    //添加一个请求等待loading
    return config
}, function(err) {
    console.error(err)
    //Do something with request error
    return Promise.reject(err)
});
//添加一个响应拦截器
ajax.interceptors.response.use(res => {

    var data = res.data

    if (data.status) {
        return data.data;
    }

    if (data.errorCode == 401) {
        console.log('请登录')
        return
    }

    return Promise.reject(data.message)
}, function(err) {
    console.log(err)
    //Do something with response error
    return Promise.reject(err)
})

export const getPicture = params => {
    return ajax.get('picture', params)
}
